var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var strip_tags = require('striptags');
var randomToken = require('random-token');
var SIDE_CONVERSION = {1: 'X', 2: 'O'};
users = [];
connections = [];
matches = [];

server.listen(process.env.PORT  || 3000);
console.log('server running');

app.get('/', function (req, res){
	res.sendFile(__dirname +  '/index.html');
});

function addConnection(socket) {
	connections.push(socket);
	console.log('Connected: %s sockets connected', connections.length);	
}

function removeConnection(socket) {
	connections.splice(connections.indexOf(socket), 1);
	console.log('disconnected: %s sockets connected', connections.length);
}

function addMatch(match) {
	matches.push(match);
	console.log('New match started between %s and %s', match.playerA.socket.username, match.playerB.socket.username);	
}

function removeMatch(match) {
	matches.splice(matches.indexOf(match), 1);
	console.log('Match between %s and %s has ended, result was: %s - %s',
			 match.playerA.socket.username,
			 match.playerB.socket.username, 
			 match.playerA.score, 
			 match.playerB.score);
}

function getMatch(id) {
	console.log("Trying to find match %s", id);
	for(var i = 0; i < matches.length; i++) {
		if (matches[i].id == id) {
			return matches[i];
		}
	}

	return null;
}

function updateUserNames(){
	io.sockets.emit('get users', users);
}

function getSocket(username) {
	for(var i = 0; i < connections.length; i++) {
		if (connections[i].username == username) {
			return connections[i];
		}
	}
	return null;
	// return Object.keys(connections).forEach(function(key) {
	// 	var socket = connections[key];
	// 	if(socket.username == username) {
	// 		return socket;
	// 	}
	// });	
}

function clearStatuses(socket) {
	socket.playing = false;
	socket.beingChallengedBy = null;
	socket.challenging = null;
}

function isChallengingOrChallenged(socket) {
	return socket.beingChallengedBy || socket.challenging;
}

function isPlaying(socket) {
	return socket.playing;
}

function isBusy(socket) {
	return isPlaying(socket) || isChallengingOrChallenged(socket);
}

function isSelf(socketA, socketB) {
	return socketA.username == socketB.username;
}

function initNewMatch(socketChallenger, socketChallengee)
{
	var match = {
			id: randomToken(16),
			playerA: {
				socket: socketChallenger,
				score: 0,
				side: 1,
				readyToContinue: true
			},

			playerB: {
				socket: socketChallengee,
				score: 0,
				side: 2,
				readyToContinue: true
			},

			gameState: {
				ended: false,
				toMove: 1,
				1: 0,
				2: 0,
				3: 0,
				4: 0,
				5: 0,
				6: 0,
				7: 0,
				8: 0,
				9: 0					
			}
	};

	return match;
}

function reinitGame(match) {	
	match.gameState = {
		ended: false,
		toMove: 1,
		1: 0,
		2: 0,
		3: 0,
		4: 0,
		5: 0,
		6: 0,
		7: 0,
		8: 0,
		9: 0					
	}
}

function startMatch(match) {
	match.playerA.socket.playing, match.playerB.socket.playing = true;
	match.playerA.socket.join(match.id);
	match.playerB.socket.join(match.id);
	io.to(match.id).emit('match started', match.id);
	broadcastRefresh(match);
}

function endMatch(match) {
	match.playerA.socket.playing, match.playerB.socket.playing = false;
	match.playerA.socket.leave(match.id);
	match.playerB.socket.leave(match.id);	
	removeMatch(match);
}

function getPlayerToMove(match, socket=true) {
	if(match.gameState.toMove == match.playerA.side) {
		return socket ? match.playerA.socket : match.playerA.socket.username;
	}
	else if(match.gameState.toMove == match.playerB.side) {
		return socket ? match.playerB.socket : match.playerB.socket.username;
	}	
	return null;
}

function incrementScore(match, winner) {
	if(winner == match.playerA.side) {
		return match.playerA.score++;
	}
	else if(winner == match.playerB.side) {
		return match.playerB.score++;
	}		
}

function resolveWinnerName(match, winner) {
	if(winner == match.playerA.side) {
		return match.playerA.socket.username;
	}
	else if(winner == match.playerB.side) {
		return match.playerB.socket.username;
	}	
	return '';
}

function isPlayersTurn(match, socket) {
	var playerToMove = getPlayerToMove(match);	
	console.log("%s wants to move. It is %s turn to move", socket.username, playerToMove.username);
	if (playerToMove.username == socket.username) {
		return true
	}
	return false;
}

function makeMove(match, move) {
	match.gameState[move] = match.gameState.toMove;
}

function togglePlayerToMove(match) {
	match.gameState.toMove ^= 3; 
}

function flipSides(match) {
	match.playerA.side ^= 3;
	match.playerB.side ^= 3;
}

function getClientFriendlyState(match) {
	var state = {
		playerA: {username: match.playerA.socket.username, score: match.playerA.score, side: match.playerA.side},
		playerB: {username: match.playerB.socket.username, score: match.playerB.score, side: match.playerB.side},
		gameState: match.gameState
	};
	console.log("player to move: %s, %s", getPlayerToMove(match, false), match.gameState.toMove);
	return state;
}

function broadcastRefresh(match) {
	var state = getClientFriendlyState(match);
	io.to(match.id).emit('match refresh', state);
}

function broadcastWinner(match, winner) {	
	io.to(match.id).emit('game winner', winner);
}

function broadcastContinue(match) {	
	io.to(match.id).emit('match continue');
}

function isCellOccupied(gameState, cell) {
	return gameState[cell] != 0;
}

function cellsAreEqual(gameState, a, b, c) {
	return gameState[a] == gameState[b] && gameState[a] == gameState[c];
}

function playersReady(match) {
	console.log("player A ready to continue: %s, player B ready to continue: %s", match.playerA.readyToContinue, match.playerB.readyToContinue);
	return match.playerA.readyToContinue && match.playerB.readyToContinue;
}

function setPlayerReady(match, socket) {
	if (match.playerA.socket.username == socket.username) {
		match.playerA.readyToContinue = true;
	}

	if (match.playerB.socket.username == socket.username) {
		match.playerB.readyToContinue = true;
	}
}	

function parseForEnd(gameState) {
	// orka, gets unreadable
	// for(var i = 1; i <= 3; i++) {
	// 	var weight = (i-1) * 3;
	// 	if(gameState[1*weight] != 0 && gameState[1*weight] == gameState[2*weight] && gameState[1*weight] == gameState[3*weight]) {

	// 	}
	// }
	if(gameState[1] != 0 && (cellsAreEqual(gameState, 1, 2, 3) || cellsAreEqual(gameState, 1, 4, 7))) {
		return gameState[1];
	}

	if(gameState[5] != 0 && (cellsAreEqual(gameState, 4, 5, 6) || cellsAreEqual(gameState, 2, 5, 8) || cellsAreEqual(gameState, 3, 5, 7)
						 || cellsAreEqual(gameState, 1, 5, 9))) {
		return gameState[5];
	}	

	if(gameState[9] != 0 && (cellsAreEqual(gameState, 7, 8, 9) || cellsAreEqual(gameState, 3, 6, 9))) {
		return gameState[9];
	}	
	return false;
}

io.sockets.on('connection', function(socket){
	addConnection(socket);

	socket.on('disconnect', function(data){
		users.splice(users.indexOf(socket.username), 1);
		updateUserNames();
		removeConnection(socket);
		clearStatuses(socket);
	});

	socket.on('send message', function(data){
		data = strip_tags(data, '');
		io.sockets.emit('new message', {msg: data, user: socket.username});
	});

	socket.on('new user', function(data, callback){
		data = strip_tags(data, '');
		socket.username = data;
		users.push(socket.username);
		updateUserNames();
		callback(true);
	});

	socket.on('send challenge', function(data){
		var socketChallengee = getSocket(data.challengee);
		//Neither socket must already be challenging someone or being challenged by someone. Also, cannot challenge self.
		//Also, must not be playing.  Also, im gonna poke your ass with my nose :^)
		if (!isBusy(socketChallengee) && !isBusy(socket) && !isSelf(socketChallengee, socket) ) {
			socketChallengee.emit('receive challenge', {challenger: socket.username});
			socketChallengee.beingChallengedBy = socket;
			socket.challenging = socketChallengee;
		}
	});

	socket.on('decline challenge', function(data){
		var socketChallenger = socket.beingChallengedBy;
		socketChallenger.emit('challenge declined', {challengee: socket.username});
		socketChallenger.challenging = null;
		socket.beingChallengedBy = null;
	});

	socket.on('accept challenge', function(data){
		var socketChallenger = socket.beingChallengedBy;
		socketChallenger.emit('challenge accepted', {challengee: socket.username});
		socketChallenger.challenging = null;
		socket.beingChallengedBy = null;
		var match = initNewMatch(socketChallenger, socket);
		addMatch(match);
		startMatch(match);
	});	

	socket.on('get match state', function(id, reply) {
		var match = getMatch(id);
		console.log("Match state of match between %s and %s was queried", match.playerA.socket.username, match.playerB.socket.username);
		var state = getClientFriendlyState(match);
		reply(state);
	});


	/* game related */
	socket.on('ready to continue', function(matchId) {
		var match = getMatch(matchId);
		setPlayerReady(match, socket);
		console.log("User %s is ready to continue the match", socket.username);
		if (playersReady(match)) {
			reinitGame(match);
			flipSides(match);
			broadcastContinue(match);
			broadcastRefresh(match);
		}
	});

	socket.on('make move', function(data, isLegal) {
		var match = getMatch(data.id);
		// console.log("match has ended %s ",match.gameState.ended);
		// console.log("cell is occupied %s", !isCellOccupied(match.gameState, data.cell));
		// console.log("is players turn %s", !isPlayersTurn(match, socket));
		// console.log(!isPlayersTurn(match, socket) || !isCellOccupied(match.gameState, data.cell) || match.gameState.ended);
		if (!isPlayersTurn(match, socket) || isCellOccupied(match.gameState, data.cell) || match.gameState.ended) {
			isLegal(false);
		}	
		else {
			isLegal(true);
			makeMove(match, data.cell);
			var winner = parseForEnd(match.gameState);
			if (winner) {
				incrementScore(match, winner);
				var winnerName = resolveWinnerName(match, winner);
				match.gameState.ended = true;
				match.playerA.readyToContinue = false;
				match.playerB.readyToContinue = false;
				broadcastRefresh(match);			
				broadcastWinner(match, winnerName);
			} 
			else {
				togglePlayerToMove(match);				
				broadcastRefresh(match);			
			}
			
		}
	});

});